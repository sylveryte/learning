/*
 * promises.js
 * Copyright (C) 2020 sylveryte <sylveryte@archred>
 *
 * Distributed under terms of the MIT license.
 */

console.log('\n\n\n--------------Start---------------------');

let fruits = [
  {
    name: 'mango',
    color: 'yellow/orange',
  },
  {
    name: 'black plum',
    color: 'black/violet/red',
  },
];

const printData = () => {
	console.log('Fruites------>')
  fruits.forEach((fruit, index) => {
    console.log(`${index} - ${fruit.name}\t ${fruit.color}`);
  });
};

const postNewFruit = () => {
	// totally moves forward if setTimeout
  setTimeout(() => {
    fruits.push({name: 'kiwi', color: 'green/brown'});
  }, 2000);
  return !fail;
};

//control outcomes this is often network result
const fail = false;

const postNewFruitPromise = new Promise((resolve, reject) => {
  if (postNewFruit()) {
    resolve();
  } else {
    reject('Something went wrong');
  }
});

postNewFruitPromise
  .then(printData)
  .then(printData) //potential another task
  .catch(e => console.log(`Error Promise : ${e}`))
.finally(()=> printData());

setTimeout(printData,2000);

console.log('--------------End----------------------\n');
