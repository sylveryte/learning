/*
 * morepromises.js
 * Copyright (C) 2020 sylveryte <sylveryte@archred>
 *

 * Distributed under terms of the MIT license.
 */
console.log('\n\n\n--------------Start---------------------');

let fruits = [
  {name: 'mango', color: 'yellow'},
  {name: 'black plum', color: 'violet'},
  {name: 'kiwi', color: 'green'},
];

const peelFruit = fruit => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (fruit.color != 'green') {
        console.log(`Peeled ${fruit.name} and shell is ${fruit.color}`);
        resolve(`peeled ${fruit.name}`);
      } else reject("ERROR: We don't peel green colored fruits");
    }, 1000);
  });
};

Promise.all([
  peelFruit(fruits[0]),
  peelFruit(fruits[1]),
  peelFruit(fruits[2]).catch(e => console.log(e)),
]).then(values => {
  console.log(values);
});

console.log('Racing')
Promise.race([
  peelFruit(fruits[0]),
  peelFruit(fruits[1]),
  peelFruit(fruits[2]).catch(e => console.log(e)),
]).then(values => {
  console.log(` fastest that was done : ${values}`);
});


console.log('--------------End----------------------\n');
