/*
 * asyncawait.js
 * Copyright (C) 2020 sylveryte <sylveryte@archred>
 *
 * Distributed under terms of the MIT license.
 */


console.log('\n\n\n--------------Start---------------------');

let fruits = [
  {name: 'mango', color: 'yellow'},
  {name: 'black plum', color: 'violet'},
  {name: 'kiwi', color: 'green'},
];

const peelFruit = fruit => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (fruit.color != 'green') {
        console.log(`Peeled ${fruit.name} and shell is ${fruit.color}`);
        resolve(`peeled ${fruit.name}`);
      } else reject("ERROR: We don't peel green colored fruits");
    }, 1000);
  });
};

// promise way
// peelFruit(fruits[0]).then(
//   peelFruit(fruits[2])
//     .then(peelFruit(fruits[1]))
//     .catch(error => console.log(error)),
// );

async function peelFruitsMan() {
  try {
    await peelFruit(fruits[0]);
    await peelFruit(fruits[1]);
    await peelFruit(fruits[2]);
  } catch (error) {
    console.log(error);
  }
}
peelFruitsMan();

console.log('peelMan is it done?');
console.log('--------------End----------------------\n');
