/*
 * callbacks.js
 * Copyright (C) 2020 sylveryte <sylveryte@archred>
 *
 * Distributed under terms of the MIT license.
 */

console.log('\n\n\n--------------Start---------------------');

let fruits = [
  {
    name: 'mango',
    color: 'yellow/orange',
  },
  {
    name: 'black plum',
    color: 'black/violet/red',
  },
];

const printData = () => {
	console.log('Fruites------>')
  fruits.forEach((fruit, index) => {
    console.log(`${index} - ${fruit.name}\t ${fruit.color}`);
  });
};

const postNewFruit = () => {
	// totally moves forward if setTimeout
  setTimeout(() => {
    fruits.push({name: 'kiwi', color: 'green/brown'});
  }, 2000);
  return !fail;
};

//control outcomes this is often network result
const fail = false;

callbackForm = (task, callback, errorcallback) => {
  const res = task();
  if (res) callback('Callback');
  else errorcallback(`failed : ${res}`);
};

callbackForm(postNewFruit, printData, error => console.log(error));


console.log('------------------End---------------------\n');
